package tuile;
/**
  * classe Droite representant les tuiles droites
  * @author lbattani
  */

public class Droite extends Tuile{
  /*
   * constante correspondant au type d'une tuile depart
   */
  private final static int DEPART=1;
  /*
   * constante correspondant au type d'une tuile arrivee
   */
  private final static int ARRIVE=2;
  /*
   * attributs statiques qui verifie respectivement l'unicite des tuiles de
   * depart et d arrivee
   */
  private static int DEPART_c,ARRIVEE_c=0;
  /*
   * entier indiquant le type de la tuile(depart,arrivee ou normal)
   */
  private int type_ADA;

  /**
    * @param nom nom de la tuile
    * @param pente pente eventuelle de la tuile
    * @param type type de la ligne droite (normale,depart,arrivee)
    *
    * @throws ADUniciteException
    */
  public Droite(char nom,int pente,int type) throws ADUniciteException{
    super(nom,6,pente);

      if(type==DEPART){
        //si le depart est deja present une exception levee
        if(DEPART_c>0)
          throw new ADUniciteException("ERROR : depart deja present");

        DEPART_c++;
      }
      if(type==ARRIVE){
        //meme cas pour l'arrivee
        if(ARRIVEE_c>0)
          throw new ADUniciteException("ERROR : arrivee deja present");

        ARRIVEE_c++;
      }
      this.type_ADA=type;
  }

  /**
    * @param nom nom de la tuile
    * @param pente pente eventuelle de la tuile
    * @param type type de la ligne droite (normale,depart,arrivee)
    * @param test chaine quelquoncque permettant de differencier les deux constructeurs
    *
    * constructeur destine a la creation du kit de tuiles de depart
    */
  protected Droite(char nom,int pente,int type,String test){
    super(nom,6,pente,test);
    this.type_ADA=type;
  }
  /**
    * @return booleen indiquant que la tuile est une ligne droite
    *
    */
  public boolean estLigneDroite(){
    return true;
  }
  /**
    *
    * @return vrai si la tuile n'est pas une arrivee ni un depart
    */
  public boolean normale(){
    return (type_ADA!=ARRIVE&&type_ADA!=DEPART);
  }
  /**
    *
    * @return vrai si la tuile est la ligne de depart
    */
  public boolean depart(){
    return (type_ADA==DEPART);
  }
  /**
    *
    * @return vrai si la tuile est la ligne d'arrivee
    */
  public boolean arrivee(){
    return (type_ADA==ARRIVE);
  }

  /**
    * @return true si les deux pieces droites sont les memes false sinon
    *
    */
  public boolean equals(Droite d){
    return(super.equals(d)&&this.type_ADA==d.type_ADA);
  }

  /**
    * @return entier symbolisant le type du depart
    *
    */
  protected static int d(){
    return DEPART_c;
  }
  /**
    * @return entier symbolisant le type de l'arrivee
    *
    */
  protected static int a(){
    return ARRIVEE_c;
  }

  /**
    * fonction statique utilisee pour les remises a zero pour une nouvelle partie
    *
    * @param d nouveau nombre de departs
    *
    */
  protected static void setD(int d){
    DEPART_c=d;
  }

  /**
    * fonction statique utilisee pour les remises a zero pour une nouvelle partie
    *
    * @param a nouveau nombre de d'arrivees
    *
    */
  protected static void setA(int a){
    ARRIVEE_c=a;
  }

}
