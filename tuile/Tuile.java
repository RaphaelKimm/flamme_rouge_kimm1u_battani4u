package tuile;

import java.io.Serializable;
/**
 * classe Tuile
 *
 * la classe tuile symbolise les tuiles qu'elles soient un virage
 * ou une ligne droite
 * @author lbattani
 */
public abstract class Tuile implements Serializable{
  /*
   * tableau de tuiles contenant les tuiles de base du jeu face recto
   */
  private final static Tuile[] kit_recto={new Droite('a',0,1,""),
                                         new Droite('b',0,0,""),
                                         new Droite('c',0,0,""),
                                         new Droite('d',0,0,""),
                                         new Virage('e',0,true,""),
                                         new Droite('f',0,0,""),
                                         new Virage('g',0,true,""),
                                         new Virage('h',0,false,""),
                                         new Virage('i',0,true,""),
                                         new Virage('j',0,true,""),
                                         new Virage('k',0,false,""),
                                         new Droite('l',0,0,""),
                                         new Droite('m',0,0,""),
                                         new Droite('n',0,0,""),
                                         new Virage('o',0,true,""),
                                         new Virage('p',0,true,""),
                                         new Virage('q',0,false,""),
                                         new Virage('r',0,false,""),
                                         new Virage('s',0,false,""),
                                         new Virage('t',0,false,""),
                                         new Droite('u',0,2,"")};
 /*
  * tableau de tuiles contenant les tuiles de base du jeu face verso
  */
  private final static Tuile[] kit_verso={new Droite('A',0,1,""),
                                         new Droite('B',1,0,""),
                                         new Droite('C',-1,0,""),
                                         new Droite('D',1,0,""),
                                         new Virage('E',1,true,""),
                                         new Droite('F',-1,0,""),
                                         new Virage('G',-1,true,""),
                                         new Virage('H',-1,false,""),
                                         new Virage('I',1,true,""),
                                         new Virage('J',1,true,""),
                                         new Virage('K',1,false,""),
                                         new Droite('L',-1,0,""),
                                         new Droite('M',1,0,""),
                                         new Droite('N',1,0,""),
                                         new Virage('O',1,true,""),
                                         new Virage('P',-1,true,""),
                                         new Virage('Q',1,false,""),
                                         new Virage('R',1,false,""),
                                         new Virage('S',-1,false,""),
                                         new Virage('T',-1,false,""),
                                         new Droite('U',1,2,"")};
/*
 * attribut statique symbolisant la montee son oppose symbolisant la descente
 */
  public final static int MONTEE=1;
  public final static long serialVersionUID=1L;
  /*
   * on stocke le nom des tuiles deja utilisees dans une chaine afin de ne pas se retrouver
   * avec des tuiles ayant le meme nom
   */
  private static String tuiles_utilise="";
  /*
   * indique si le mode sandbox est active ou pas, c-a-d si l'utilisateur peut placer
   * autant de tuiles et comme il le souhaite
   */
  private static boolean sandbox;
/*
 * nom servant a la construction des pistes a l'aide des indications des cartes
 * etape
 */
  private char nom;
/*
 * nombre de case de la tuile
 */
  private int p_case;
/*
 * entier indiquant la pente de la tuile 1 si montee -1 si descente 0 sinon
 */
  private int pente;

  /**
    * @param nom nom de la tuile
    * @param c nombre de case de la tuile
    * @param pente pente eventuelle de la tuile
    * @param test chaine quelquoncque permettant de differencier les deux constructeurs
    *
    * constructeur destine a la creation du kit de tuiles de depart
    */
  protected Tuile(char nom,int c,int pente,String test){
    this.pente=pente;
    this.p_case=c;
    this.nom=nom;
  }

/**
  * @param nom nom de la tuile
  * @param c nombre de case de la tuile
  * @param pente pente eventuelle de la tuile
  *
  * @throws NameFormatException
  */
  public Tuile(char nom,int c,int pente) throws NameFormatException{
    this.pente=pente;
    this.p_case=c;
    String s=""+nom;
    s=s.toLowerCase();
    int testmin=((int)s.charAt(0)-(int)'a');

    if((testmin<0||testmin>25)&&!sandbox)
      throw new NameFormatException("ERROR "+nom+" n\'est pas un caractere autorise");

    if(tuiles_utilise.contains(s)&&!sandbox)
      throw new ADUniciteException("ERROR la piece "+nom+" est deja presente");

    tuiles_utilise+=s;
    this.nom=nom;
  }

  /**
    * @param c lettre dont on veut trouver la tuile correspondante
    * @return tuile correspondant a la lettre passee en parametre
    * @throws ADUniciteException
    *
    */
  public static Tuile getKit(char c) throws ADUniciteException{
    String s=""+c;
    char c2=c;
    s=s.toLowerCase();
    c=s.charAt(0);

    //si le recto ou le verso de la tuile est deja utilise et que le mode sandbox est desactive une exception est levee
    if(Tuile.tuiles_utilise.contains(s)&&!sandbox)
      throw new ADUniciteException("ERROR la piece "+c+" est deja presente");


      if((int)c-(int)'a'>=0&&(int)c-(int)'a'<26){
        Tuile.tuiles_utilise+=s;
        //si la tuile est droite on test si c'est une arrivee ou un depart
        if(Tuile.kit_recto[(int)c-(int)'a'] instanceof Droite){
          //si le depart est deja utilise une exception est levee
          if(Droite.d()!=0&&((Droite)Tuile.kit_recto[(int)c-(int)'a']).depart())
            throw new ADUniciteException("ERROR depart deja present");
          //si l'arrivee est deja utilisee une exception est levee
          if(Droite.a()!=0&&((Droite)Tuile.kit_recto[(int)c-(int)'a']).arrivee())
            throw new ADUniciteException("ERROR arrivee deja presente");

          //on met a jour le nombre de depart ou d'arrivee en fonction du type de la tuile
          if(((Droite)Tuile.kit_recto[(int)c-(int)'a']).depart()){
            Droite.setD(1);
          }else if(((Droite)Tuile.kit_recto[(int)c-(int)'a']).arrivee()){
            Droite.setA(1);
          }
        }
      }
      if((int)c2-(int)'a'>=0&&(int)c2-(int)'a'<26)
        return Tuile.kit_recto[(int)c2-(int)'a'];
      if((int)c2-(int)'A'>=0&&(int)c2-(int)'A'<26)
        return Tuile.kit_verso[(int)c2-(int)'A'];
      else
        return null;
      }

  /**
    * permet de remettre a zero les tuiles utilisees pour une nouvelle partie
    *
    */
  public static void reset(){
    Tuile.tuiles_utilise="";
    Droite.setA(0);
    Droite.setD(0);
  }
  /**
    * active le mode sandbox
    */
  public static void enableSandbox(){
    Tuile.sandbox=true;
  }
  /**
    * desactive le mode sandbox
    */
  public static void disableSandbox(){
    Tuile.sandbox=false;
  }

  /**
    * @return booleen indiquant si la tuile est une montee
    *
    */
  public boolean estMontee(){
    return (pente==MONTEE);
  }

  /**
    * @return booleen indiquant si la tuile est une descente
    *
    */
  public boolean estDescente(){
    return (pente==-MONTEE);
  }

  /**
    * @return nombre de case de la tuile
    *
    */
  public int longueur(){
    return this.p_case;
  }

  /**
    * @return nom de la tuile
    *
    */
  public char nom(){
    return this.nom;
  }

  /**
    * @return true si les deux tuiles sont identiques
    *
    */
  public boolean equals(Tuile t){
    return (this.nom==t.nom&&this.pente==t.pente&&this.p_case==t.p_case);
  }

  /**
    * @return booleen indiquant vrai si la tuile est droite faux la tuile est un virage
    *
    */
  public abstract boolean estLigneDroite();

}
