package tuile;
/**
 * Exception levee quand le nom d'une tuile ne correspond pas au format du jeu
 *  @author rkimm
 */
public class NameFormatException extends RuntimeException{
  public NameFormatException(){
    super();
  }
  public NameFormatException(String s){
    super(s);
  }
}
