package tuile;
/**
  * classe Droite representant les tuiles virage
  */
public class Virage extends Tuile{
  /**
    * booleen indiquant vrai si le virage est serre et faux si il est leger
    * @author rkimm
    */
  private boolean serre;

  /**
    * @param nom nom de la tuile
    * @param pente pente eventuelle de la tuile
    * @param serre indique si le virage est serre ou leger
    *
    */
  public Virage(char nom,int pente,boolean serre){
    super(nom,2,pente);
    this.serre=serre;
  }

  /**
    * @param nom nom de la tuile
    * @param pente pente eventuelle de la tuile
    * @param serre indique si le virage est serre ou leger
    * @param test chaine quelquoncque permettant de differencier les deux constructeurs
    *
    * constructeur destine a la creation du kit de tuiles de depart
    */
  protected Virage(char nom,int pente,boolean serre,String test){
    super(nom,2,pente,test);
    this.serre=serre;
  }

  /**
    * @return vrai si le virage est serre
    */
  public boolean serre(){
    return this.serre;
  }

  /**
    * @return vrai si le virage est leger
    */
  public boolean leger(){
    return !this.serre;
  }

  /**
    * @return booleen indiquant vrai si la tuile est droite faux la tuile est un virage
    *
    */
  public boolean estLigneDroite(){
    return false;
  }

  /**
    * @return true si les deux virages sont identiques
    *
    */
  public boolean equals(Virage v){
    return(super.equals(v)&&this.serre==v.serre);
  }

}
