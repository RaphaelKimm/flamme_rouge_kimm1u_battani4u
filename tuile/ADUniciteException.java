package tuile;
/**
  * Exception levee lorsqu'il y a duplicite de l'arrivee ou du depart
  *@author rkimm
  */
public class ADUniciteException extends RuntimeException{
  public ADUniciteException(){
    super();
  }
  public ADUniciteException(String s){
    super(s);
  }
}
