package jeu;
/**
  * exception levée lorsqu'un numéro de carte piochée par un cycliste est incorrect
  * @author rkimm
  */
public class IncorrectNumberException extends Exception
{
  /**
    * @param s message de l'exception
    *
    */
  public IncorrectNumberException(String s)
  {
    super(s);
  }
}
