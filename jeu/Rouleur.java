package jeu;
/**
 * classe representant les Rouleurs
 *  @author rkimm
 */
public class Rouleur extends Cycliste
{
  /**
    constructeur initialisant un rouleur par rapport à sa position actuelle et à sa position sur une file

    * @param p entier, position actuelle du cycliste sur une tuile
    * @param f booléen, indique sur quelle file le cycliste est présent (gauche/droite)
    * @param id id du cycliste
  */
  public Rouleur(int p, boolean f,int id)
  {
    super(p,f,id);
  }
}
