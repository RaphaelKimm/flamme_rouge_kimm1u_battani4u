package jeu;
/**
  * exception utilise pour indiquer le cycliste franchissant l'arrivee en premier
  * @author lbattani
  */
public class VictoireException extends Exception{
  /*
   * id du cycliste victorieux
   */
  private int cycl;
  /**
    * @param cycl cycliste victorieux
    *
    */
  public VictoireException(Cycliste cycl){
    super();
    this.cycl=cycl.getID();
  }
  /**
    * @return id du cycliste victorieux
    *
    */
  public int getCycl(){
    return cycl;
  }
}
