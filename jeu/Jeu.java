package jeu;

import java.util.Stack;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import java.sql.SQLException;

import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Serializable;

import piste.*;
import cartes.*;
import tuile.*;
/**
  * classe Jeu
  * @author lbattani,rkimm
  *
  *
  */

public class Jeu implements Serializable{
  private ArrayList<Joueur> listejoueur;
  private Stack<CarteFatigue> packetsprinteur;
  private Stack<CarteFatigue> packetrouleur;
  private Circuit piste;
  private Scanner scann=new Scanner(System.in);
  private String phasedejeu;
  private static String PATH="D:/projetsnotés";
  private final static String PATH2="/flamme_rouge/save/";
  private final static String SAVENAME=".save";
  public final static long serialVersionUID=1L;

  public Jeu(){
    this.listejoueur=new ArrayList<>(4);
    this.piste=null;
    this.phasedejeu="finale";
    this.packetsprinteur=new Stack<>();
    this.packetrouleur=new Stack<>();
    for(int i=0;i<30;i++){
      this.packetsprinteur.push(new CarteFatigue());
      this.packetrouleur.push(new CarteFatigue());
    }
  }

  public void demarrerJeu(){
    try{
      this.reset();
      this.lancerJeu();
    }catch(ExtensionException ee){
      System.out.println(ee.getMessage());
      this.demarrerJeu();
    }catch(ADUniciteException ad){
      System.out.println(ad.getMessage());
      this.demarrerJeu();
    }catch(IOException ioe){
      System.out.println(ioe.getMessage());
      this.demarrerJeu();
    }catch(ClassNotFoundException cnfe){
      System.out.println(cnfe.getMessage());
      this.demarrerJeu();
    }
  }

  public void reset(){
    Tuile.reset();
    Cycliste.reset();
  }

  private void lancerJeu() throws ExtensionException,IOException,ClassNotFoundException{
    String choix="";
    while(!choix.equals("y")&&!choix.equals("n")){
      System.out.print("charger un sauvegarde ?(y/n):");
      choix=scann.nextLine();
      System.out.println("\n");
    }
    if(choix.equals("n")){
      System.out.print("entrer le nom d'un fichier park ou sandbox : ");
      String name=scann.nextLine();
      System.out.print("\n");
      this.piste=new Circuit(name);
      System.out.print("nombre de joueur :");
      String nb1=scann.nextLine();
      int nb=Integer.parseInt(nb1);
      System.out.println("\nentrer les cyclistes sous la forme : equipe-pseudo-position_1-file_1-position_2-file_2");
      if(nb<2||nb>4)
        nb=4;
      for(int i=0;i<nb;i++){
        String format=scann.nextLine();
      listejoueur.add(new Joueur(format.split("-")[1],format.split("-")[0],Integer.parseInt(format.split("-")[2]),format.split("-")[3].contains("droite"),Integer.parseInt(format.split("-")[4]),format.split("-")[5].contains("droite")));
      }
    }else{
      System.out.print("entrer le nom de la sauvegarde: ");
      String name=scann.nextLine();
      System.out.println("\n");
      Jeu j=Jeu.reprendre(name);
      this.piste=j.piste;
      this.phasedejeu=j.phasedejeu;
      this.packetsprinteur=j.packetsprinteur;
      this.packetrouleur=j.packetrouleur;
      for(Joueur jo:j.listejoueur)
        this.listejoueur.add(new Joueur(jo.getPseudo(),jo.getEquipe(),jo.getSprinteur().getPos(),jo.getSprinteur().getFile(),jo.getRouleur().getPos(),jo.getRouleur().getFile()));
    }
    try{
      while(true)
        this.passerPhaseSuivante();
    }catch(VictoireException ve){
      for(int i=0;i<listejoueur.size();i++){
          System.out.println("---------------------");
          System.out.println(listejoueur.get(ve.getCycl()-listejoueur.get(0).getID()).getPseudo()+" A GAGNE !!!");
          System.out.println("---------------------");
        if(i==ve.getCycl()-listejoueur.get(0).getID())
          listejoueur.get(i).gagne();
        else
          listejoueur.get(i).perd();
        }
      }
  }

  private List<Cycliste> listeCyclistesTrierParPosition(){
    return Cycliste.getAllCycliste();
  }

  private void printNum(){
    for(int i=0;i<piste.longueur();i++)
      System.out.print(" "+i%10);
    System.out.print("\n");
  }

  private void printpiste(){
    for(int i=0;i<piste.longueur();i++){
      if(!piste.estVirage(i)&&piste.posVoieNormale(i))
        System.out.print("==");
      else if(piste.estVirage(i)&&piste.posVoieNormale(i))
        System.out.print("))");
      else if(!piste.estVirage(i)&&piste.posDescente(i))
        System.out.print("<<");
      else if(piste.estVirage(i)&&piste.posDescente(i))
        System.out.print("[[");
      else if(!piste.estVirage(i)&&!piste.posDescente(i)&&!piste.posVoieNormale(i))
        System.out.print(">>");
      else
        System.out.print("]]");
    }
    System.out.print("\n");
  }

  private void printjoueur(boolean file){
    int i=0;
    for(Cycliste c : this.listeCyclistesTrierParPosition()){
      for(int j=i;j<c.getPos();j++)
        System.out.print("  ");
      if(c.getFile()==file){
        System.out.print(c.getID()%4);
        if(c instanceof Rouleur)
          System.out.print("R");
        else
          System.out.print("S");
      }else
        System.out.print("  ");
      i=c.getPos()+1;
    }
    System.out.print("\n");
  }

  private void afficherPositionsCyclistes(){
    this.printNum();
    this.printpiste();
    this.printjoueur(false);
    this.printjoueur(true);
    this.printpiste();
    System.out.print("\n");
  }

  private void piocher(Cycliste c) throws IncorrectNumberException{
    ArrayList<Carte> pioche_1=c.piocher4Cartes();
    System.out.print("choisir une carte (1 a "+pioche_1.size()+"):");
    String str=scann.nextLine();
    c.choisirCarte(pioche_1,Integer.parseInt(str)-1);
    System.out.println("carte choisie: avance="+c.getCurrentCard().getNum());
  }

  private void piocherN(Cycliste c){
    try{
      this.piocher(c);
    }catch(IncorrectNumberException ine){
      System.out.println(ine.getMessage());
      this.piocherN(c);
    }
  }

  private void piocherCarte(){
    for(Joueur j:listejoueur){
      String choice="";
      while(!choice.equals("sprinteur")&&!choice.equals("rouleur")){
        System.out.print(j.getPseudo()+" deplacer le rouleur ou le sprinteur ?(taper rouleur/sprinteur):");
        choice=scann.nextLine();
        System.out.print("\n");
      }
      switch(choice){
        case "sprinteur":
        {
          this.piocherN(j.getSprinteur());
        }
        {
          this.piocherN(j.getRouleur());
        }
        break;
        case "rouleur":
        {
          this.piocherN(j.getRouleur());
        }
        {
          this.piocherN(j.getSprinteur());
        }
        break;
        default:
        break;
      }

    }
  }

  private void avancerCycliste() throws VictoireException{
    for(Joueur j:listejoueur){
      j.getSprinteur().avancer(piste);
      j.getRouleur().avancer(piste);
    }
  }

  private void phasemouvement() throws VictoireException{
    System.out.println("\n");
    this.afficherPositionsCyclistes();
    this.avancerCycliste();
    System.out.println("\n");
    this.afficherPositionsCyclistes();
  }

  private void appliquerAspiration(){
    int rng=0;
    int tmp=0;
    int grp=0;
    while(rng<this.listeCyclistesTrierParPosition().size()){
      if(rng<this.listeCyclistesTrierParPosition().size()-1){
        if(this.listeCyclistesTrierParPosition().get(rng).getPos()==this.listeCyclistesTrierParPosition().get(rng+1).getPos()){
          rng++;
          grp++;
        }
        if(Cycliste.occupe(this.listeCyclistesTrierParPosition().get(rng).getPos()+1))
          grp++;
        else{
          if(Cycliste.occupe(this.listeCyclistesTrierParPosition().get(rng).getPos()+2)){
            for(int i=tmp;i<=grp+tmp&&i<this.listeCyclistesTrierParPosition().size();i++)
                this.listeCyclistesTrierParPosition().get(i).positionActuelle++;
          }else{
            tmp=grp+1;
            grp=0;
          }
        }
      }
      rng++;
    }
  }

  private void appliquerFatigue(){
    for(Cycliste c : this.listeCyclistesTrierParPosition()){
      if(!Cycliste.occupe(c.getPos()+1)){
        if(c instanceof Sprinteur && !packetsprinteur.empty())
          c.ajouterFatigue(packetsprinteur.pop());
        else if(!packetrouleur.empty())
          c.ajouterFatigue(packetrouleur.pop());
      }
    }
  }

  private void phasefinale(){
    this.appliquerAspiration();
    this.appliquerFatigue();
    System.out.println("\n");
    this.afficherPositionsCyclistes();
  }

  private void passerPhaseSuivante() throws VictoireException{
    switch(phasedejeu){
      case "energie":
        this.phasemouvement();
        phasedejeu="mouvement";
      break;

      case "mouvement":
        this.phasefinale();
        phasedejeu="finale";
      break;

      case "finale":
        this.piocherCarte();
        phasedejeu="energie";
      break;

      default:
      break;
    }
    String choice="";
    while(!choice.equals("y")&&!choice.equals("n")){
      System.out.print("sauvegarder ? (y/n): ");
      choice=scann.nextLine();
      System.out.println("\n");
    }
    if(choice.equals("y")){
      System.out.print("entrer le nom de la sauvegarde: ");
      String name=scann.nextLine();
      System.out.println("\n");
      try{
        this.sauve(name);
      }catch(IOException ioe){}
    }
  }

  public void sauve(String name) throws IOException{
    ObjectOutputStream oos=new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File(PATH+PATH2+name+SAVENAME))));
    oos.writeObject(this);
    oos.close();
  }

  public static Jeu reprendre(String name) throws IOException,ClassNotFoundException{
    ObjectInputStream ois=new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(PATH+PATH2+name+SAVENAME))));
    Jeu j=((Jeu)ois.readObject());
    ois.close();
    return j;
  }

}
