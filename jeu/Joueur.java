package jeu;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.CallableStatement;
import java.sql.Types;

import java.io.Serializable;
/**
  * classe Joueur
  * @author rkimm
  */
public class Joueur implements Serializable{
  /*
   * attribut static comptant le nombre d'instance de Joueur,sert à l'attribution des ids des cyclistes
   */
  private static int NB=0;
  /*
   * id servant a differencier les joueur
   */
  private int id;
  /*
   * pseudo du joueur
   */
  private String pseudo;
  /*
   * nom de l'equipe du joueur
   */
  private String equipe;
  /*
   * sprinteur du joueur
   */
  private Sprinteur sprinteur;
  /*
   * rouleur du joueur
   */
  private Rouleur rouleur;
  public final static long serialVersionUID=1L;

  /**
    * @param pseudo pseudo du joueur
    * @param equipe nom de l'equipe du joueur
    * @param pos1 position du sprinteur
    * @param file1 file du sprinteur
    * @param pos2 position du rouleur
    * @param file2 file du rouleur
    *
    *
    *
    */
  public Joueur(String pseudo,String equipe,int pos1,boolean file1,int pos2,boolean file2){
    this.pseudo=pseudo;
    this.equipe=equipe;
    this.id=NB;
    NB++;
    this.sprinteur=new Sprinteur(pos1,file1,this.id);
    this.rouleur=new Rouleur(pos2,file2,this.id);
    try{
    DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
    Connection connect=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1522:XE","login","mdp");
    //on verifie si le joueur est deja inscrit dans la base
    CallableStatement callstat=connect.prepareCall("{?=call is_present(?)}");
    callstat.registerOutParameter(1,Types.INTEGER);
    callstat.setString(2,pseudo);
    callstat.execute();
    //si il n'est pas inscrit l'inscription est effectuee
    if(callstat.getInt(1)==0){
      CallableStatement ajout=connect.prepareCall("{call addjoueur(?,?,?)}");
      ajout.registerOutParameter(3,Types.INTEGER);
      ajout.setString(1,this.pseudo);
      ajout.setString(2,this.equipe);
      ajout.execute();
      ajout.close();
    }
    callstat.close();
    connect.close();
  }catch(SQLException sqle){
  }
  }
  /**
    * @return id du joueur
    */
  public int getID(){
    return id;
  }
  /**
    * @return pseudo du joueur
    */
  public String getPseudo(){
    return pseudo;
  }
  /**
    * @return nom d'equipe du joueur
    */
  public String getEquipe(){
    return equipe;
  }
  /**
    * @return sprinteur du joueur
    */
  public Sprinteur getSprinteur(){
    return sprinteur;
  }
  /**
    * @return rouleur du joueur
    */
  public Rouleur getRouleur(){
    return rouleur;
  }
  /**
    * inscrit la victoire d'un joueur dans la base
    *
    */
  public void gagne(){
    try{
    Connection connect=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1522:XE","login","mdp");
    CallableStatement callstat=connect.prepareCall("{call win(?,?)}");
    callstat.registerOutParameter(2,Types.INTEGER);
    callstat.setString(1,this.pseudo);
    callstat.execute();
    callstat.close();
    connect.close();
  }catch(SQLException sqle){}
  }
  /**
    * inscrit la defaite d'un joueur dans la base
    *
    */
  public void perd(){
    try{
      Connection connect=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1522:XE","login","mdp");
      CallableStatement callstat=connect.prepareCall("{call loose(?,?)}");
      callstat.registerOutParameter(2,Types.INTEGER);
      callstat.setString(1,this.pseudo);
      callstat.execute();
      callstat.close();
      connect.close();
    }catch(SQLException sqle){}
  }
  /**
    * affiche la table des scores
    * @throws SQLException
    */
  public static void afficheScore() throws SQLException{
    Connection connect=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1522:XE","login","mdp");
    Statement s_equipe=connect.createStatement();
    PreparedStatement ps_joueur=connect.prepareStatement("SELECT pseudo FROM joueur WHERE equipe=?");
    ResultSet equipe=s_equipe.executeQuery("SELECT DISTINCT equipe FROM joueur");
    CallableStatement cs_solo=connect.prepareCall("{call give_w_l(?,?,?)}");
    CallableStatement cs_team=connect.prepareCall("{call give_w_l_team(?,?,?)}");
    ResultSet joueur=null;
    cs_solo.registerOutParameter(2,Types.INTEGER);
    cs_solo.registerOutParameter(3,Types.INTEGER);
    cs_team.registerOutParameter(2,Types.INTEGER);
    cs_team.registerOutParameter(3,Types.INTEGER);
    //affichage des joueurs par equipe
    while(equipe.next()){
      cs_team.setString(1,equipe.getString("equipe"));
      cs_team.execute();
      System.out.println("Equipe "+equipe.getString("equipe")+" victoires:"+cs_team.getInt(2)+" defaites:"+cs_team.getInt(3));
      ps_joueur.setString(1,equipe.getString("equipe"));
      joueur=ps_joueur.executeQuery();
      while(joueur.next()){
        cs_solo.setString(1,joueur.getString("pseudo"));
        cs_solo.execute();
        System.out.println(" --"+joueur.getString("pseudo")+"-victoires:"+cs_solo.getInt(2)+"-defaites:"+cs_solo.getInt(3));
      }
      System.out.println(" ");
    }
    cs_team.close();
    cs_solo.close();
    joueur.close();
    ps_joueur.close();
    equipe.close();
    s_equipe.close();
    connect.close();
  }
}
