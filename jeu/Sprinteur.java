package jeu;
/**
 * classe representant les Sprinteurs
 *  @author rkimm
 */
public class Sprinteur extends Cycliste
{
  /**
    constructeur initialisant un sprinteur par rapport à sa position actuelle et à sa position sur une file

    * @param p entier, position actuelle du cycliste sur une tuile
    * @param f booléen, indique sur quelle file le cycliste est présent (gauche/droite)
    * @param id id du cycliste
  */
  public Sprinteur(int p, boolean f,int id)
  {
    super (p,f,id);
  }
}
