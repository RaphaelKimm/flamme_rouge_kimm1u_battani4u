package jeu;
import java.util.*;
import java.io.Serializable;
import cartes.*;
import piste.*;

/**
*  classe Cycliste
*  représente l'ensemble des cyclistes d'une course, c'est-à-dire les sprinteurs et les rouleurs du joueur
* @author rkimm,lbattani
*/
public abstract class Cycliste implements Comparable<Cycliste>,Serializable
{
  /*
    position actuelle du cycliste dans le circuit (entier)
  */
  protected int positionActuelle;

  /*
    précise si le joueur est sur la file de droite ou la file de gauche (booléen)
    0 si sur file gauche, et 1 si sur file droite
  */
  protected boolean fileDroite;

  /*
    indique la carte choisie par le joueur lors de la phase énergie (entier)
  */
  private Carte carteChoisie;
  /*
   * id permettant de differencier les cyclistes
   */
  private int id;
  /*
    liste de cartes defaussées du joueur
  */
  private ArrayList<Carte> listeCartesDefauses;

  /*
    liste de cartes faces cachee du joueur
  */
  private ArrayList<Carte> listeCartesFacesCachees;

  /*
    liste des positions des cyclistes
  */
  private static ArrayList<Cycliste> LISTECYC=new ArrayList<>(8);
  /*
   * constante indiquant le nombre de carte de départ d'un cycliste
   */
  private final static int MAXCARTE=15;
  public final static long serialVersionUID=1L;

  /**
    constructeur initialisant un cycliste par rapport à sa position actuelle et à sa position sur une file

    * @param p entier, position actuelle du cycliste sur une tuile
    * @param f booléen, indique sur quelle file le cycliste est présent (gauche/droite)
    * @param id id du cycliste
  */
  public Cycliste(int p, boolean f,int id)
  {
    //ajout des attributs
    this.positionActuelle=p;
    this.fileDroite=f;
    this.id=id;
    //ajout d'une reference vers this dans la liste des cyclistes
    LISTECYC.add(this);
    Collections.sort(LISTECYC);
    this.listeCartesDefauses=new ArrayList<Carte>();
    this.listeCartesFacesCachees=new ArrayList<Carte>();
    //le cycliste pioche ses 15 cartes
    for(int i=0;i<MAXCARTE;i++)
      this.listeCartesFacesCachees.add(new CarteEnergie());
  }

  /**
   méthode permettant de piocher 4 cartes pour un rouleur/sprinteur (joueur)

   * @return liste, ArrayList<Carte> : liste représentant les 4 cartes piochées par le joueur
  */
  public ArrayList<Carte> piocher4Cartes()
  {
    ArrayList<Carte> liste=new ArrayList<Carte>(4);
    int taille=4;
    //si il n'y a plus de cartes face cachee on melange les cartes de la defausse et on les place dans les cartes face cachee
    if(this.listeCartesFacesCachees.isEmpty()){
      for(int i=0;i<10;i++){
        int pos_1=((int)Math.random()*this.listeCartesDefauses.size());
        int pos_2=((int)Math.random()*this.listeCartesDefauses.size());
        while(pos_1==pos_2)
          pos_2=((int)(Math.random()*this.listeCartesDefauses.size()));
        this.echanger(pos_1,pos_2);
      }
      this.listeCartesFacesCachees.addAll(this.listeCartesDefauses);
      this.listeCartesDefauses.clear();
    }
    //si il reste moins de 4 cartes face cachee on pioche toutes les cartes restantes
    if(this.listeCartesFacesCachees.size()<4)
      taille=this.listeCartesFacesCachees.size();
    //ajout des cartes à la pioche
    for(int i=0;i<taille;i++)
      liste.add(this.listeCartesFacesCachees.get(i));
    return liste;
  }
  /**
    * methode qui echange deux cartes dans la défausse, utilisee afin de melanger la défausse quand il n'y a plus de cartes face cachee
    *
    * @param pos_1 position de la premiere carte a echanger
    * @param pos_2 position de la deuxieme carte a echanger
    *
    */
  private void echanger(int pos_1,int pos_2){
    Carte c=this.listeCartesDefauses.get(pos_1);
    this.listeCartesDefauses.set(pos_1,this.listeCartesDefauses.get(pos_2));
    this.listeCartesDefauses.set(pos_2,c);
  }

  /**
    * @param cf carte fatigue qu'on ajoute dans la defausse du cycliste
    *
    */
  public void ajouterFatigue(CarteFatigue cf){
    this.listeCartesDefauses.add(((int)Math.random()*this.listeCartesDefauses.size()),cf);
  }

  /**
    * méthode utilisee pour comparer deux cycliste en fonction de leur position et ainsi trier la liste des cyclistes
    *
    * @param cyc cycliste qu'on veut comparer à this
    *
    */
  public int compareTo(Cycliste cyc){
    return this.positionActuelle-cyc.positionActuelle;
  }

  /**
    * @return liste des cyclistes
    *
    */
  public static ArrayList<Cycliste> getAllCycliste(){
    return LISTECYC;
  }

  /**
    * @return position du cycliste
    *
    */
  public int getPos(){
    return positionActuelle;
  }

  /**
    * @return booléen indiquant la file du cycliste
    *
    */
  public boolean getFile(){
    return fileDroite;
  }
  /**
    * methode utilisee pour les tests
    *
    * @return les cartes face cachee du cycliste
    *
    */
  public ArrayList<Carte> facescachees(){
    return listeCartesFacesCachees;
  }
  /**
    * methode utilisee pour les tests
    *
    * @return les cartes dans la defausse du cycliste
    *
    */
  public ArrayList<Carte> defausse(){
    return listeCartesDefauses;
  }
  /**
    * @return id du cycliste
    *
    */
  public int getID(){
    return id;
  }
  /**
    * remet a zero la liste des cycliste pour une nouvelle partie
    *
    */
  public Carte getCurrentCard(){
    return carteChoisie;
  }
  /**
    * remet la liste des cyclistes a zero pour une nouvelle partie
    *
    */
  public static void reset(){
    LISTECYC.clear();
  }

  /**
    *
    * @param pos position dont on veut savoir si elle est occupee
    * @return true si la position en parametre est occupee
    *
    */
  static boolean occupe(int pos){
    for(Cycliste c : LISTECYC){
      if(c.positionActuelle==pos)
        return true;
    }
    return false;
  }

  /**
    méthode permettant de choisir une carte dans une liste selon le numéro choisi par le joueur

    * @param l ArrayList<Carte>, liste de cartes du joueur
    * @param num entier, numéro de la carte souhaitée par le joueur (entre 1 et 4)
    * @throws IncorrectNumberException exception levée lorsque le numéro de la carte est incorrect
    *
  */
  public void choisirCarte(ArrayList<Carte> l, int num) throws IncorrectNumberException
  {
    if (!l.isEmpty())
    {
      if(num<=4&&num>=l.size())
        num=l.size()-1;

      if (num>=0 && num<l.size())
      {
        // on ajoute les cartes dans les listes correspondantes, notamment pour défausser les cartes non jouées
        this.carteChoisie=l.get(num);
        l.remove(num);
        for (int i=0;i<l.size();i++)
        {
          if (!l.get(i).equals(this.carteChoisie)){
            this.listeCartesDefauses.add(l.get(i));
            this.listeCartesFacesCachees.remove(l.get(i));
          }
        }
      }
      else
        // si le numéro est inférieur à 0 ou supérieur à la taille de la pioche, on lève une exception
        throw new IncorrectNumberException("Erreur : le numero de la carte doit etre entre 1 et "+l.size());
    }
  }

  /**
    * methode qui fait avancer le cycliste et jette une exception si il passe la ligne d'arrivee
    *
    * @param c circuit sur lequel le cycliste avance
    *
    * @throws VictoireException
    */
  public void avancer(Circuit c) throws VictoireException
  {

    int pos=this.positionActuelle;
    int avance=this.carteChoisie.getNum();
    boolean done=false;
    boolean empty=false;
    boolean right=true;
    int park=LISTECYC.indexOf(this);
    //on parcour le circuit du point de depart au point d'arrivee
    for(int i=pos;i<pos+avance;i++){
      //si la position est sur l'arrivee on leve une exception indiquant que le cycliste a gagne
      if(c.posArrive(i)){
        this.positionActuelle=i;
        throw new VictoireException(this);
      }
      //si la position n'est pas sur un plat et que la pente n'a pas ete prise en compte on modifie ou pas la distance que va parcourir le cycliste en fonction de la pente de la tuile sur laquelle il se trouve
      if(!c.posVoieNormale(i)&&!done){
        done=true;
        if((avance<5&&c.posDescente(i))||(avance>5&&!c.posVoieNormale(i)&&!c.posDescente(i)))
          avance=5;
        //si le point d'arrivee du cycliste est sur ou apres l'arrivee on leve une exception pour indiquer qu'il a gagne
        if(pos+avance>c.longueur()-2){
          this.positionActuelle=c.longueur()-2;
          throw new VictoireException(this);
        }
      }
    }
    boolean second=false;
    boolean first=false;
    boolean tmp=false;
    //on parcoure tant que la position du point d'arrivee est totalement occupee
    while(!empty){
      second=false;
      tmp=false;
      while(park<LISTECYC.size()){
        if(this!=LISTECYC.get(park)&&LISTECYC.get(park).positionActuelle==pos+avance){
          //si le point d'arrivee possede deja un cycliste on met a jour le booléen l'indiquant
          if(!second){
            second=true;
            first=true;
            tmp=LISTECYC.get(park).fileDroite;
          //si le point d'arrivee est totalement occupe et si le cycliste ne recule pas par rapport à sa position de depart le cycliste recule
          }else if(avance!=0){
            avance--;
            first=false;
            break;
          }else{
            empty=true;
            break;
          }
        }
        park++;
      }
      //si le point de depart possede deja un cycliste on place le cycliste dans la file opposee
      if(second&&first){
        this.positionActuelle+=avance;
        this.fileDroite=(!tmp);
        Collections.sort(LISTECYC);
        break;
      //sinon il va automatiquement dans la file de droite
    }else if(!second){
        this.positionActuelle+=avance;
        this.fileDroite=true;
        Collections.sort(LISTECYC);
        break;
      }
      park=LISTECYC.indexOf(this);
    }
 }
}
