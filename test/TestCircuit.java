package test;

import piste.*;
import tuile.*;
/**
  * @author rkimm
  *
  */
public class TestCircuit{
  public static void main(String[] args) {
    //test a partir d'un nom de fichier inexistant, leve une exception
    try{
      Circuit c1=new Circuit("test.txt");
    }catch(Exception e){
      e.printStackTrace();
    }
    //test avec un fichier ayant la mauvaise extension pour la creation d'un circuit
    try{
      Circuit c1=new Circuit("test_1.txt");
    }catch(Exception e){
      e.printStackTrace();
    }
    //test avec un fichier sans extension
    try{
      Circuit c1=new Circuit("test_2");
    }catch(Exception e){
      e.printStackTrace();
    }
    //test avec un fichier park contenant plusieurs fois la meme piece
    try{
      Circuit c1=new Circuit("test_3.park");
    }catch(Exception e){
      e.printStackTrace();
    }
    Tuile.reset();
    //test avec un fichier sandbox contenant plusieurs departs
    try{
      Circuit c1=new Circuit("test_4.sandbox");
    }catch(Exception e){
      e.printStackTrace();
    }
    Tuile.reset();

    Circuit c2=null;
    //test sur un fichier park ne contenant pas d'erreur de synthaxe
    try{
      c2=new Circuit("test_5.park");
    }catch(Exception e){
      e.printStackTrace();
    }
    Tuile.reset();

    //test des fonction sur un fichier sandbox valide
    try{
      Circuit c3=new Circuit("test_6.sandbox");
      System.out.println("la position 11 est sur la tuile p : "+c3.estALaPosition(11,Tuile.getKit('p')));
      System.out.println("la position 13 est sur la tuile p : "+c3.estALaPosition(13,Tuile.getKit('p')));
      System.out.println("la position 1 est en descente : "+c3.posDescente(1));
      System.out.println("la position 1 est en plat : "+c3.posVoieNormale(1));
      System.out.println("la position 13 est en descente : "+c3.posDescente(13));
      System.out.println("la position 13 est en plat : "+c3.posVoieNormale(13));
    }catch(Exception e){
      e.printStackTrace();
    }
    Tuile.reset();
  }
}
