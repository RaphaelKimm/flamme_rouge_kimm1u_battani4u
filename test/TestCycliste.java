package test;

import jeu.*;
import piste.*;
import tuile.*;
import cartes.*;
import java.util.ArrayList;
/**
  *
  * @author lbattani
  */
public class TestCycliste{
  public static void main(String[] args) {
    Circuit c2=null;
    //circuit correct utilise pour tester les cyclistes
    try{
      //on utilise un fichier sandbox pour des tests plus nets
      c2=new Circuit("test_6.sandbox");
    }catch(Exception e){
      e.printStackTrace();
    }
    //on crée 5 cyclistes
    Cycliste cy1=new Sprinteur(2,false,1);
    Cycliste cy2=new Rouleur(2,true,2);
    Cycliste cy3=new Rouleur(1,false,3);
    Cycliste cy4=new Sprinteur(1,true,4);
    Cycliste cy5=new Rouleur(3,true,5);
    //test du triage par position des cyclistes
    ArrayList<Cycliste> listeTest=Cycliste.getAllCycliste();
    for(Cycliste c : listeTest){
      System.out.print("position: "+c.getPos()+" file de ");
      if(c.getFile())
        System.out.println("droite");
      else
        System.out.println("gauche");
    }
    //verification de la pile de carte d'un cycliste en debut de partie
    ArrayList<Carte> cartes=cy1.facescachees();
    for(Carte c1:cartes)
      System.out.println("carte "+c1.getNum());

    //test de la methode de pioche de carte et des methodes utilisees en jeu
    for(int i=0;i<6;i++){
      try{
        ArrayList<Carte> pioche=cy1.piocher4Cartes();
        System.out.println("cartes de la pioche :");
        for(Carte c : pioche)
          System.out.println("carte "+c.getNum());
        System.out.println(" ");
        cy1.choisirCarte(pioche,i%4);
        System.out.println("la carte choisie est une carte "+cy1.getCurrentCard().getNum());

        System.out.println("cartes face cachee :");
        for(Carte c : cy1.facescachees())
          System.out.println("carte "+c.getNum());
        System.out.println(" ");

        System.out.println("cartes de la defausse :");
        for(Carte c : cy1.defausse())
          System.out.println("carte "+c.getNum());
        System.out.println(" ");
      }catch(Exception e){
        e.printStackTrace();
      }
    }

    //test de la methode avancer
    //simulation d'une partie de 12 tours ou les cartes sont piochées au hasard
    try{
    for(int i=0;i<12;i++){
      // cycliste 2
      {
        System.out.println("CYCLISTE 2\n");
        ArrayList<Carte> pioche=cy2.piocher4Cartes();
        System.out.println("cartes de la pioche :");
        for(Carte c : pioche)
          System.out.println("carte "+c.getNum());
        System.out.println(" ");
        ArrayList<Carte> pioche_2=new ArrayList<>();
        boolean b=false;
        b=pioche_2.addAll(pioche);
        cy2.choisirCarte(pioche,i%4);
        System.out.println("la carte choisie est une carte "+cy2.getCurrentCard().getNum());

        System.out.println("cartes face cachee :");
        for(Carte c : cy2.facescachees())
          System.out.println("carte "+c.getNum());
        System.out.println(" ");

        System.out.println("cartes de la defausse :");
        for(Carte c : cy2.defausse())
          System.out.println("carte "+c.getNum());
        System.out.println(" ");
        cy2.avancer(c2);

        for(Cycliste c : listeTest){
          System.out.print("position: "+c.getPos()+" file de ");
          if(c.getFile())
            System.out.println("droite");
          else
            System.out.println("gauche");
        }
      }
      //cycliste 3
        {
          System.out.println("CYCLISTE 3\n");
          ArrayList<Carte> pioche=cy3.piocher4Cartes();
          System.out.println("cartes de la pioche :");
          for(Carte c : pioche)
            System.out.println("carte "+c.getNum());
          System.out.println(" ");
          ArrayList<Carte> pioche_2=new ArrayList<>();
          boolean b=false;
          b=pioche_2.addAll(pioche);
          cy3.choisirCarte(pioche,i%4);
          System.out.println("la carte choisie est une carte "+cy3.getCurrentCard().getNum());

          System.out.println("cartes face cachee :");
          for(Carte c : cy3.facescachees())
            System.out.println("carte "+c.getNum());
          System.out.println(" ");

          System.out.println("cartes de la defausse :");
          for(Carte c : cy3.defausse())
            System.out.println("carte "+c.getNum());
          System.out.println(" ");
          cy3.avancer(c2);

          for(Cycliste c : listeTest){
            System.out.print("position: "+c.getPos()+" file de ");
            if(c.getFile())
              System.out.println("droite");
            else
              System.out.println("gauche");
          }
        }

      //cycliste 4
        {
          System.out.println("CYCLISTE 4\n");
          ArrayList<Carte> pioche=cy4.piocher4Cartes();
          System.out.println("cartes de la pioche :");
          for(Carte c : pioche)
            System.out.println("carte "+c.getNum());
          System.out.println(" ");
          ArrayList<Carte> pioche_2=new ArrayList<>();
          boolean b=false;
          b=pioche_2.addAll(pioche);
          cy4.choisirCarte(pioche,i%4);
          System.out.println("la carte choisie est une carte "+cy4.getCurrentCard().getNum());

          System.out.println("cartes face cachee :");
          for(Carte c : cy4.facescachees())
            System.out.println("carte "+c.getNum());
          System.out.println(" ");

          System.out.println("cartes de la defausse :");
          for(Carte c : cy4.defausse())
            System.out.println("carte "+c.getNum());
          System.out.println(" ");
          cy4.avancer(c2);

          for(Cycliste c : listeTest){
            System.out.print("position: "+c.getPos()+" file de ");
            if(c.getFile())
              System.out.println("droite");
            else
              System.out.println("gauche");
          }
        }

      //cycliste 5

      {
          System.out.println("CYCLISTE 5\n");
          ArrayList<Carte> pioche=cy5.piocher4Cartes();
          System.out.println("cartes de la pioche :");
          for(Carte c : pioche)
            System.out.println("carte "+c.getNum());
          System.out.println(" ");
          ArrayList<Carte> pioche_2=new ArrayList<>();
          boolean b=false;
          b=pioche_2.addAll(pioche);
          cy5.choisirCarte(pioche,i%4);
          System.out.println("la carte choisie est une carte "+cy5.getCurrentCard().getNum());

          System.out.println("cartes face cachee :");
          for(Carte c : cy5.facescachees())
            System.out.println("carte "+c.getNum());
          System.out.println(" ");

          System.out.println("cartes de la defausse :");
          for(Carte c : cy5.defausse())
            System.out.println("carte "+c.getNum());
          System.out.println(" ");
          cy5.avancer(c2);

          for(Cycliste c : listeTest){
            System.out.print("position: "+c.getPos()+" file de ");
            if(c.getFile())
              System.out.println("droite");
            else
              System.out.println("gauche");
          }
        }
    }
  }catch(Exception e){
    e.printStackTrace();
    if(e instanceof VictoireException)
      //on capture la VictoireException pour savoir quel cycliste a gagne
      System.out.println("LE CYCLISTE "+((VictoireException)e).getCycl()+" A GAGNE !!!");
  }
    Tuile.reset();
  }
}
