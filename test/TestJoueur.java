package test;

import java.sql.SQLException;
import jeu.Joueur;
/**
  *
  *@author rkimm
  */
public class TestJoueur{
  public static void main(String[] args) {
    Joueur j=null;
    Joueur a=null;
    Joueur o=null;
    try{
      j=new Joueur("jean_kevin","les_kikoos",2,true,2,false);
      a=new Joueur("thetroll78","les_kikoos",1,false,3,false);
      o=new Joueur("le_roi_des_bambis","team_bambi",1,true,3,true);
    }catch (SQLException sqle){
      sqle.printStackTrace();
    }
    try{
      j.gagne();
      j.gagne();
      j.gagne();
      j.gagne();
      a.gagne();
      o.gagne();
      o.gagne();
      j.perd();
      a.perd();
      a.perd();
      a.perd();
    }catch (SQLException sqle){
      sqle.printStackTrace();
    }
    try{
      Joueur.afficheScore();
    }catch (SQLException sqle){
      sqle.printStackTrace();
    }
  }
}
