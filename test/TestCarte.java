package test;

import cartes.*;
/**
  * classe TestCarte : tests de la classe Carte et de ses classes filles
  * @author rkimm
  */
public class TestCarte{

  public static void main(String[] args) {
    //creation d une carte fatigue
    Carte c1=new CarteFatigue();
    //verification que le numero de la carte fatigue est bien 2
    System.out.println(c1.getNum());
    //on realise 10 tirages de cartes energie pour verifier que le numero est bien dans l intervalle [INF;SUP] et est aleatoire
    for(int i=0;i<10;i++){
      Carte c2=new CarteEnergie();
      System.out.println(c2.getNum());
    }
  }

}
