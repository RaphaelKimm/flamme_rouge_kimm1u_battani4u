package test;

import tuile.*;
/**
  *
  *@author lbattani
  */
public class TestTuile{

  public static void main(String[] args) {
    //creation d une tuile droite en arrivee
    Tuile d1=new Droite('a',1,2);
    //creation d'une deuxieme tuile d'arrivee, leve une exception
    try{
      Droite d2=new Droite('b',-1,2);
    }catch(Exception e){
      e.printStackTrace();
    }
    //creation d'une tuile a partir d'un caractere non-autorise, leve l'exception correspondante
    try{
      Droite d3=new Droite('!',0,0);
    }catch(Exception e){
      e.printStackTrace();
    }
    //tentative de creation d'une deuxieme tuile a qui leve une exception
    try{
      Tuile t1=new Droite('A',0,0);
    }catch(Exception e){
      e.printStackTrace();
    }
    //on active cette fois ci le mode sandbox aucune exception n est levee
    Tuile.enableSandbox();
    try{
      Tuile t1=new Droite('A',0,0);
    }catch(Exception e){
      e.printStackTrace();
    }
    //desactivation du mode sandbox
    Tuile.disableSandbox();
    //remise a zero des cartes
    Tuile.reset();
    //deuxieme tentative l instruction ne leve plus l'exception
    try{
      Tuile t1=new Droite('A',0,0);
    }catch(Exception e){
      e.printStackTrace();
    }

    //on pioche la tuile F
    Tuile t2=Tuile.getKit('F');
    System.out.println(t2.nom());

  }

}
