DROP TABLE joueur CASCADE CONSTRAINTS;

CREATE TABLE joueur(
  pseudo VARCHAR2(70),
  equipe VARCHAR2(70),
  victoires NUMBER(3),
  defaites NUMBER(3)
);

ALTER TABLE joueur
  ADD CONSTRAINT pkjoueur PRIMARY KEY(
  pseudo
);

CREATE OR REPLACE
  PROCEDURE addjoueur(psd IN joueur.pseudo%TYPE,eq IN joueur.equipe%TYPE,error_code OUT INTEGER)
    IS
    BEGIN
      INSERT INTO joueur VALUES (psd,eq,0,0);
      error_code:=0;
      EXCEPTION
        WHEN
          DUP_VAL_ON_INDEX
            THEN
              error_code:=1;
    END;

  CREATE OR REPLACE
    PROCEDURE win(psd IN joueur.pseudo%TYPE,error_code OUT INTEGER)
      IS
      BEGIN
        UPDATE joueur
          SET victoires=victoires+1
          WHERE pseudo=psd;
        error_code:=0;
        EXCEPTION
          WHEN NO_DATA_FOUND
            THEN
              error_code:=1;
      END;

  CREATE OR REPLACE
    PROCEDURE loose(psd IN joueur.pseudo%TYPE,error_code OUT INTEGER)
      IS
      BEGIN
        UPDATE joueur
          SET defaites=defaites+1
          WHERE pseudo=psd;
        error_code:=0;
        EXCEPTION
          WHEN NO_DATA_FOUND
            THEN
              error_code:=1;
      END;

  CREATE OR REPLACE
    PROCEDURE give_w_l(psd IN joueur.pseudo%TYPE,win OUT NUMBER,loose OUT NUMBER)
      IS
        BEGIN
          SELECT victoires,defaites INTO win,loose
            FROM joueur
              WHERE pseudo=psd;
          EXCEPTION
            WHEN
              NO_DATA_FOUND
                THEN
                  win:=0;
                  loose:=0;
        END;

  CREATE OR REPLACE
    PROCEDURE give_w_l_team(team IN joueur.equipe%TYPE,win OUT NUMBER,loose OUT NUMBER)
      IS
        w NUMBER(4);
        l NUMBER(4);
        CURSOR psd(t joueur.equipe%TYPE)
          IS
            SELECT pseudo
              FROM joueur
                WHERE equipe=t
                ORDER BY pseudo ASC;
      BEGIN
        win:=0;
        loose:=0;
        FOR pseudo_row IN psd(team) LOOP
          give_w_l(pseudo_row.pseudo,w,l);
          win:=win+w;
          loose:=loose+l;
        END LOOP;
      END;

  CREATE OR REPLACE
    FUNCTION is_present(psd VARCHAR2)
      RETURN INTEGER
        IS
          cmpt INTEGER;
        BEGIN
          SELECT count(*) INTO cmpt
            FROM joueur
              WHERE pseudo=psd;
          IF cmpt=0
            THEN
              RETURN 0;
            ELSE
              RETURN 1;
          END IF;
        END is_present;

  COMMIT;
