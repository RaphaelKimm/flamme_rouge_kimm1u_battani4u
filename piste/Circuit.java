package piste;

import tuile.*;
import java.util.ArrayList;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;

/**
 * classe representant les circuits, c-a-d une suite de tuiles positionnées les unes
 * a la suite des autres
 * @author lbattani
 */
public class Circuit implements Serializable{
  /*
   * nom du circuit
   */
  private String nom;
  /*
   * tuiles composant le circuit
   */
  private ArrayList<Tuile> circuit;
  /*
   * nombre de case du circuit
   */
  private int length=0;
  public final static long serialVersionUID=1L;

  /**
    * @param savename nom du fichier a partir duquel on cree le circuit
    *
    * @throws IOException
    * @throws ExtensionException
    */
  public Circuit(String savename) throws IOException,ExtensionException{
    //on separe le nom du circuit et l'extension du fichier
    String[] file=savename.split("\\.");
    this.nom=file[0];
    this.circuit=new ArrayList<Tuile>();
    //si le fichier n'existe pas l'exception correspondante est levee
    if(!(new File(savename)).exists())
      throw new FileNotFoundException("ERROR "+savename+" not found");

    try{
      //selon l'extension du fichier on active ou pas le mode sandbox
      switch(file[1]){
        case "park":
          Tuile.disableSandbox();
        break;

        case "sandbox":
          Tuile.enableSandbox();
        break;

        default:
          //une exception est levee si le fichier n'a pas la bonne extension
          throw new ExtensionException("ERROR mauvaise extension de "+savename);
      }
    }catch(ArrayIndexOutOfBoundsException npe){
      throw new ExtensionException("ERROR mauvaise extension de "+savename);
    }

    int key=0;
    //ouverture du fichier
    FileReader fr=new FileReader(savename);

    while(key!=-1){
      key=fr.read();
      char c=' ';
      //passage de l'unicode au code ascii puis au char
      if(key-97>=0&&key-97<21){
        c=((char)(key-97+((int)('a'))));
      }else if(key-65>=0&&-65<21){
        c=((char)(key-65+((int)('A'))));
      }
      if(key!=-1){
        //ajout de la tuile du kit de depart correspondant a la lettre dechiffree
        Tuile t=Tuile.getKit(c);
      if(t!=null){
        circuit.add(t);
        length+=t.longueur();
      }
      }
    }

    fr.close();

  }

  /**
    * @param i dont on veut trouver la tuile
    * @return Tuile a la position i
    *
    */
  private Tuile tuileALaPosition(int i){
    int j=0;
    while(i>this.circuit.get(j).longueur()){
      i=i-this.circuit.get(j).longueur();
      j++;
      if(j>=circuit.size()){
        j=circuit.size()-1;
      }
    }
    return this.circuit.get(j);
  }

  /**
    * @param c position dont on veut savoir si elle se trouve sur la tuile passee en parametre
    * @param t tuile dont on veut savoir si elle contient la position c
    *
    * @return vrai si c se trouve sur la tuile t
    */
  public boolean estVirage(int c){
    return !this.tuileALaPosition(c).estLigneDroite();
  }
  /**
    * @param c position dont on veut savoir si elle se situe en descente
    *
    * @return true si la position est en descente false sinon
    */
  public boolean posDescente(int c){
    return this.tuileALaPosition(c).estDescente();
  }
  /**
    * @param c position dont on veut savoir si elle se situe sur un plat
    *
    * @return true si la position se trouve sur un plat false sinon
    */
  public boolean posVoieNormale(int c){
    return !this.tuileALaPosition(c).estMontee()&&!this.tuileALaPosition(c).estDescente();
  }
  /**
    * @param c position dont on veut savoir si elle se situe apres ou sur l'arrivee
    *
    * @return true si la position se situe apres ou sur l'arrivee false sinon
    */
  public boolean posArrive(int c){
    if(this.tuileALaPosition(c).estLigneDroite())
      return ((Droite)this.tuileALaPosition(c)).arrivee()&&(length-this.tuileALaPosition(c).longueur())+length-c<2;
    else
      return false;
  }
  /**
    * @return longueur du circuit
    *
    */
  public int longueur(){
    return length;
  }

}
