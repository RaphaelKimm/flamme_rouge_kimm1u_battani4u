package piste;
/**
  * exception gerant les problemes lies au extension des fichiers
  * @author lbattani
  */
public class ExtensionException extends Exception{
  public ExtensionException(){
    super();
  }
  public ExtensionException(String s){
    super(s);
  }
}
