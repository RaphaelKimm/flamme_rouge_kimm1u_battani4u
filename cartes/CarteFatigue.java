package cartes;
/**
  * classe CarteFatigue representant les cartes fatigue
  * @author lbattani
  */
public class CarteFatigue extends Carte{
  /**
    * les cartes fatigues on systematiquement le numero 2
    */
  public CarteFatigue(){
    super(2);
  }
}
