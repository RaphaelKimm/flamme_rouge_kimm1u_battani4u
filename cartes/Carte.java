package cartes;

import java.io.Serializable;
/**
 * classe Carte
 * @author lbattani
 */
public abstract class Carte implements Serializable{
  /*
   * numero donnant le nombre de case a parcourir quand on pioche une carte
   */
  private int num;
  private final static long serialVersionUID=1L;
  /**
    * @param num numero de la carte
    *
    */
  public Carte(int num){
    this.num=num;
  }
  /**
    * @return numero de la carte
    *
    */
  public int getNum(){
    return num;
  }

}
