package cartes;

/**
  * classe CarteEnergie qui represente les cartes energie
  * @author lbattani
  */
public class CarteEnergie extends Carte{
 /**
   * borne inferieure pour la valeur des Cartes Energie
   */
  private final static int INF=3;
  /**
    * borne superieure pour la valeur des Cartes Energie
    */
  private final static int SUP=8;

  /**
    * le numero de la carte est tire au hasard entre INF et SUP(bornes comprises)
    *
    */
  public CarteEnergie(){
    super(INF+(int)(Math.random()*(SUP-INF)));
  }

}
